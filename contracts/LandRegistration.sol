// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

contract LandRegistration {
    uint256 public landsCounts = 0;

    struct Land {
        uint256 _id;
        uint256 plot_no;
        uint256 size;
        string location;
        bool status;
        uint256 owner_id;
        uint256 value;
        uint256 currentTime;
    }
    mapping(uint256 => Land) public lands;

    constructor() {}

    function createProperty(
        uint256 _plotId,
        string memory _location,
        uint256 _size,
        uint256 _owner_id,
        uint256 _value
    ) public returns (Land memory) {
        landsCounts++;
        lands[landsCounts] = Land(
            landsCounts,
            _plotId,
            _size,
            _location,
            true,
            _owner_id,
            _value,
            block.timestamp
        );
        return lands[landsCounts];
    }

    function findProperty(uint256 _plotId) public view returns (Land memory) {
        return lands[_plotId];
    }

    function transferProperty(
        uint256 _id,
        uint256 _owner_id,
        uint256 _value
    ) public returns (Land memory) {
        lands[_id].owner_id = _owner_id;
        lands[_id].value = _value;
        lands[_id].status = false;
        lands[_id].currentTime = block.timestamp;
        return lands[_id];
    }
}
