// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

contract OwnerRegistration {
    uint256 public ownersCounts = 0;

    struct Owner {
        uint256 _id;
        uint256 cid;
        string name;
        string add;
        bool is_owner;
    }
    mapping(uint256 => Owner) public owners;

    function createOwner(
        uint256 _cid,
        string memory _name,
        string memory _add
    ) public returns (Owner memory) {
        ownersCounts++;
        owners[ownersCounts] = Owner(ownersCounts, _cid, _name, _add, false);
        return owners[ownersCounts];
    }

    constructor() {}
}
