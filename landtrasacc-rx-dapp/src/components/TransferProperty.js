import React, { Component } from 'react'

class TransferProperty extends Component {
    render() {
        return (
            <form className="p-5 border py-3" onSubmit={(event) => {
                event.preventDefault()
                // this.props.land(
                //     this.plot_no.value,
                //     this.location.value,
                //     this.value.value,
                //     this._id.value
                // )
                this.props.transferProperty(this._id.value, this.owner.value, this.offer.value)
            }}>
                <h2 class="bg-primary alert"> Buy Land Properties</h2>
                <select id='landid' className="form-select mb-2"
                    ref={(input) => { this._id = input; }}>
                    <option defaultValue={true}>Open this select menu</option>
                    {
                        this.props.land.map((land, key) => {
                            return (
                                <option value={land._id}>{land._id} Plot Number-{land.plot_no} -- location: {land.location} -- Expected Price: Nu {land.value}</option>
                            )
                        })
                    }
                </select>
                <input id="offer" type="number"
                    ref={(input) => { this.offer = input; }}
                    className="form-control m-1"
                    placeholder="Enter Your offer in Bhutanese currency(Nu)"
                    required
                />
                <select id="owner" type="select" className="form-control select" ref={(input) => { this.owner = input; }} required>
                    <option value="">Please select the owner</option>
                    <option value="1">Samuel</option>
                    <option value="2">Phub</option>
                    <option value="3">Tshering</option>

                </select>
                <input className="form-control btn-warning" type="submit" hidden="" value="Quote your price" />
            </form>
        );
    }
}
export default TransferProperty;