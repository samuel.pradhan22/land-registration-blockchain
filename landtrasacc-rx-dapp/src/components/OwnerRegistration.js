import React, { Component } from 'react'
class OwnerRegistration extends Component {
  render() {
    return (
      <form className="p-5 border" onSubmit={(event) => {
        event.preventDefault()
        this.props.createOwner(this.cid.value, this.owner.value, this.add.value)
      }}>
        <h2>Owner Registration</h2>
        <input id="newCID" type="text"
          ref={(input) => { this.cid = input; }}
          className="form-control m-1"
          placeholder="CID"
          required
        />
        <input id="newOwner" type="text"
          ref={(input) => { this.owner = input; }}
          className="form-control m-1"
          placeholder="Name"
          required
        />

        <input id="newAddr" type="text"
          ref={(input) => { this.add = input; }}
          className="form-control m-1"
          placeholder="Address"
          required
        />
        <input className="form-control btn-primary" type="submit" hidden="" />
      </form>
    );
  }
}
export default OwnerRegistration;
