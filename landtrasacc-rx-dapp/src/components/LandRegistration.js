import React, { Component } from 'react'

class LandRegistration extends Component {
    render() {
        return (
            <form className="p-5 border py-3" onSubmit={(event) => {
                event.preventDefault()
                this.props.createProperty(this.plot_no.value, this.size.value, this.location.value, this.owner.value, this.landValue.value)
                event.target.reset();
            }}>
                <h2 class="bg-primary alert">Register Your Land Properties</h2>
                <input id="plot_no" type="number"
                    ref={(input) => { this.plot_no = input; }}
                    className="form-control m-1"
                    placeholder="Enter Your Land Plot Number"
                    required
                />
                <input id="size" type="number"
                    ref={(input) => { this.size = input; }}
                    className="form-control m-1"
                    placeholder="Enter your plot size"
                    required
                />

                <input id="location" type="text"
                    ref={(input) => { this.location = input; }}
                    className="form-control m-1"
                    placeholder="Enter your property location"
                    required
                />
                <select id="owner" type="select" className="form-control select" ref={(input) => { this.owner = input; }} required>
                    <option value="">Please select the owner</option>
                    <option value="1">Samuel</option>
                    <option value="2">Phub</option>
                    <option value="3">Tshering</option>

                </select>
                <input id="landValue" type="number"
                    ref={(input) => { this.landValue = input; }}
                    className="form-control m-1"
                    placeholder="What's your Property value in Bhutanese currency (Nu)"
                    required
                />
                <input className="form-control btn-warning" type="submit" hidden="" />
            </form>
        );
    }
}
export default LandRegistration;