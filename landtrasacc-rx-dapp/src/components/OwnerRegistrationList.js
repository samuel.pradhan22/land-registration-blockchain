import React, { Component } from 'react'
import Web3 from 'web3'
import { OWNER_REGISTRATION_ABI, OWNER_REGISTRATION_ADDRESS } from '../abi/config_ownerRegistration';
//We replace function with a component for better management of code
class OwnerRegistrationList extends Component {
    //componentDidMount called once on the client after the initial render
    //when the client receives data from the server and before the data is displayed.
    componentDidMount() {
        if (!window.ethereum)
            throw new Error("No crypto wallet found. Please install it.");
        window.ethereum.send("eth_requestAccounts");
        this.loadBlockchainData()
    }
    //create a web3 connection to using a provider, MetaMask to connect to
    //the ganache server and retrieve the first account
    async loadBlockchainData() {
        const web3 = new Web3(Web3.givenProvider || "http://localhost:7545")
        const accounts = await web3.eth.getAccounts()
        this.setState({ account: accounts[0] })

        //Step 4: load all the lists from the blockchain
        const ownerList = new web3.eth.Contract(
            OWNER_REGISTRATION_ABI, OWNER_REGISTRATION_ADDRESS)
        this.setState({ ownerList })

        const ownerCount = await ownerList.methods.ownersCounts().call()
        console.log(ownerCount);
        this.setState({ ownerCount })

        this.state.owners = []

        for (var i = 1; i <= ownerCount; i++) {
            const owner = await ownerList.methods.owners(i).call()
            this.setState({
                owners: [...this.state.owners, owner]
            })
        }

    }
    constructor(props) {
        super(props)
        this.state = {
            account: '',
            ownerCount: 0,
            owners: [],
            loading: true
        }
    }
    render() {
        return (
            <div className="container p-3">
                <h1 className="alert bg-primary" role="alert">Owner Registration List</h1>
                <table className="table">
                    <thead>
                        <tr>
                            <th>CID</th>
                            <th>Name</th>
                            <th>Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.owners.map((owner, key) => {
                                return (
                                    <tr>
                                        <td>{owner.cid}</td>
                                        <td>{owner.name}</td>
                                        <td>{owner.add}</td>
                                    </tr>
                                )
                            }
                            )
                        }
                    </tbody>
                </table>

            </div>
        );
    }
}
export default OwnerRegistrationList;


