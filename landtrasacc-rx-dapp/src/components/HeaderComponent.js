import { Component } from "react";
import { Link } from "react-router-dom";
class HeaderComponent extends Component {
    render() {
        return (
            <header className="header">
                <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                    <div><a className="navbar-brand" href="/">LRS</a></div>
                    <ul className="navbar-nav">

                        <li><Link className="nav-link" to="/owner-registration">Owner Registration</Link></li>
                        <li><Link className="nav-link" to="/owner-registration-list">View Owner List</Link></li>
                        <li><Link className="nav-link" to="/land-registration">Land Registration</Link></li>
                        <li><Link className="nav-link" to="/buy-land">Buy Land</Link></li>
                    </ul>
                </nav>
            </header>
        )
    }
}
export default HeaderComponent

