//Step 2: change the base code to use Component
//Step 2: change the base code to use Component
import React, { Component } from 'react'
import Web3 from 'web3'
import './App.css';
import OwnerRegistration from './components/OwnerRegistration';
import LandRegistration from './components/LandRegistration';
import TransferProperty from './components/TransferProperty';
import { LAND_LIST_ABI, LAND_LIST_ADDRESS } from './abi/config_landRegistration';
import { OWNER_REGISTRATION_ABI, OWNER_REGISTRATION_ADDRESS } from './abi/config_ownerRegistration';
import HeaderComponent from './components/HeaderComponent';
import withNavigation from './components/withNavigation';
import { BrowserRouter as Router, Link, Route, Routes } from "react-router-dom"
import OwnerRegistrationList from './components/OwnerRegistrationList'
//We replace function with a component for better management of code
class App extends Component {
  //componentDidMount called once on the client after the initial render
  //when the client receives data from the server and before the data is displayed.
  componentDidMount() {
    if (!window.ethereum)
      throw new Error("No crypto wallet found. Please install it.");
    window.ethereum.send("eth_requestAccounts");
    this.loadBlockchainData()
  }
  //create a web3 connection to using a provider, MetaMask to connect to
  //the ganache server and retrieve the first account
  async loadBlockchainData() {
    const web3 = new Web3(Web3.givenProvider || "http://localhost:7545")
    const accounts = await web3.eth.getAccounts()
    this.setState({ account: accounts[0] })

    //Step 4: load all the lists from the blockchain
    const landList = new web3.eth.Contract(
      LAND_LIST_ABI, LAND_LIST_ADDRESS)
    this.setState({ landList })

    const landCount = await landList.methods.landsCounts().call()
    console.log(landCount);
    this.setState({ landCount })

    const ownerList = new web3.eth.Contract(
      OWNER_REGISTRATION_ABI, OWNER_REGISTRATION_ADDRESS)
    this.setState({ ownerList })

    this.state.lands = []

    const ownerCount = await ownerList.methods.ownersCounts().call()
    console.log(ownerCount);
    this.setState({ ownerCount })

    this.state.owners = []

    for (var i = 1; i <= landCount; i++) {
      const land = await landList.methods.lands(i).call()
      this.setState({
        lands: [...this.state.lands, land]
      })
    }

    for (var i = 1; i <= ownerCount; i++) {
      const owner = await ownerList.methods.owners(i).call()
      this.setState({
        owners: [...this.state.owners, owner]
      })
    }


  }
  //Initialise the variables stored in the state
  constructor(props) {
    super(props)
    this.state = {
      account: '',
      landCount: 0,
      lands: [],
      ownerCount: 0,
      owners: [],
      loading: true
    }
    this.createProperty = this.createProperty.bind(this)
    this.transferProperty = this.transferProperty.bind((this))
    this.createOwner = this.createOwner.bind(this)
  }
  createProperty(plot_no, location, size, owner, landValue) {
    this.setState({ loading: true })
    this.state.landList.methods.createProperty(plot_no, size, location, owner, landValue)
      .send({ from: this.state.account })
      .once('receipt', (receipt) => {
        this.setState({ loading: false })
        this.loadBlockchainData()
      })
  }
  transferProperty(id, newOwner, value) {
    this.setState({ loading: true })
    this.state.landList.methods.transferProperty(id, newOwner, value)
      .send({ from: this.state.account })
      .once('receipt', (receipt) => {
        this.setState({ loading: false })
        this.loadBlockchainData()
      })
  }
  createOwner(cid, name, add) {
    this.setState({ loading: true })
    this.state.ownerList.methods.createOwner(cid, name, add)
      .send({ from: this.state.account })
      .once('receipt', (receipt) => {
        this.setState({ loading: false })
        this.loadBlockchainData()
      })
  }

  //Display the first account
  render() {
    const HeaderComponentWithNavigation = withNavigation(HeaderComponent)
    return (
      <>
        <Router>
          <HeaderComponentWithNavigation />

          <div className="container p-3">
            {/* <h1 className="alert bg-primary" role="alert">owner Registration</h1> */}
            <h2 className='alert alert-primary'>Land Registration Record</h2>
            <h4>Your Wallet Account: <span className="badge bg-secondary">{this.state.account}</span></h4>
            <Routes>
              <Route path="/owner-registration" element={<OwnerRegistration createOwner={this.createOwner} />} />
              <Route path="/owner-registration-list" element={<OwnerRegistrationList />} />
              <Route path="/land-registration" element={<LandRegistration createProperty={this.createProperty} />} />
              <Route path="/buy-land" element={<TransferProperty land={(this.state.lands)} transferProperty={this.transferProperty} />} />

            </Routes>


          </div>
        </Router>
        <div className="container p-3">
          {/* <h1 class="alert bg-primary" role="alert">Land Registration</h1> */}
          {/* <LandRegistration
            createProperty={this.createProperty} /> */}


          <ul id="landList" className="list-unstyled">
            {
              //This gets the each student from the studentList
              //and pass them into a function that display the
              //details of the student

              this.state.lands.map((land, key) => {
                return (
                  <div>
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">id</th>
                          <th scope="col">Plot Number</th>
                          <th scope="col">Plot Size (in acres)</th>
                          <th scope="col">Location</th>
                          <th scope="col">Land Value (in Nu)</th>
                          <th scope="col">Transaction Time</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>{land._id}</td>
                          <td>{land.plot_no}</td>
                          <td>{land.size}</td>
                          <td>{land.location}</td>
                          <td>{land.value}</td>
                          <td>{Date()}</td>
                        </tr>

                      </tbody>
                    </table>
                  </div>


                )
              }
              )
            }
          </ul>
          {/* <TransferProperty land={(this.state.lands)} transferProperty={this.transferProperty} /> */}
        </div>
      </>
    );
  }
}
export default App;