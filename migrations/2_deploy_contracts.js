var OwnerRegistration = artifacts.require("./OwnerRegistration");
var LandRegistration = artifacts.require("./LandRegistration.sol");
module.exports = function (deployer) {
    deployer.deploy(LandRegistration);
    deployer.deploy(OwnerRegistration);
};
